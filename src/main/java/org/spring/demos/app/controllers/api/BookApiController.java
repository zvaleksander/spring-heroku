package org.spring.demos.app.controllers.api;

import java.util.List;

import org.spring.demos.app.entities.Book;
import org.spring.demos.app.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class BookApiController {

	@Autowired
	private BookRepository bookRepository;
		
	@RequestMapping(value = "/books", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<Book> index() {
		return bookRepository.findAll();
	}
}
