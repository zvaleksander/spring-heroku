package org.spring.demos.app.controllers;

import org.spring.demos.app.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BookController {

	@Autowired
	private BookRepository bookRepository;
	
	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public String main(Model model) {
		model.addAttribute("books", bookRepository.findAll());
		
		return "book/app";
	}
	
	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public String index(Model model) {
		
		return "book/index";
	}
	
	@RequestMapping(value = "/books/{id}/show", method = RequestMethod.GET)
	public String show(@PathVariable(name = "id", required = true) Long id, Model model) {
		model.addAttribute("book", bookRepository.findById(id).orElse(null));
		
		return "book/show";
	}
}
